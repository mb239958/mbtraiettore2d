# -*- coding: iso-8859-1 -*-
"""******************************************************************
 *   � 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   py-traiettorie_scena2o.py 
 * 
 ******************************************************************"""
import numpy as np 
import matplotlib
matplotlib.use('Agg') # per potere lanciare lo script in terminale putty al di fuori di ambiente grafico
from matplotlib import pyplot as plt 
import os.path
from PIL import Image, ImageDraw, ImageEnhance, ImageFont

np_ifopt  = np.genfromtxt('OUT_ifopt_traiettoria_scena2o.csv', dtype=[('x_ifopt','f8'),('y__ifopt','f8')], delimiter=';')
np_nlopt  = np.genfromtxt('OUT_nlopt_traiettoria_scena2o.csv', dtype=[('x_ifopt','f8'),('y__ifopt','f8')], delimiter=';')
np_alglib = np.genfromtxt('OUT_alglib_traiettoria_scena2o.csv', dtype=[('x_ifopt','f8'),('y__ifopt','f8')], delimiter=';')

x = []
y = []
for punto in np_ifopt:
    x.append(punto[0])
    y.append(punto[1])
x1 = []
y1 = []
for punto in np_nlopt:
    x1.append(punto[0])
    y1.append(punto[1])
x2 = []
y2 = []
for punto in np_alglib:
    x2.append(punto[0])
    y2.append(punto[1])


plt.xlabel('x axis') 
plt.ylabel('y axis') 

circle1 = plt.Circle((1.00, 0.50), 0.12, color='r')
circle2 = plt.Circle((1.00, 0.75), 0.12, color='r')
circle3 = plt.Circle((1.00, 0.25), 0.12, color='r')
circle4 = plt.Circle((0.50, 0.50), 0.12, color='r')

fig, ax = plt.subplots() # note we must use plt.subplots, not plt.subplot
ax.set_xlim((0, 2))
ax.set_ylim((0, 1.55))

ax.add_artist(circle1)
ax.add_artist(circle2)
ax.add_artist(circle3)
ax.add_artist(circle4)

plt.title('Matteo Bigliardi UNIMORE - Analisi Traiettorie - ifopt/nlopt/alglib') 

plt.plot(x,y, color='maroon', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x1,y1, color='blue', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x2,y2, color='black', marker=',', linestyle='solid', linewidth=1, markersize=1)

plt.savefig('tmp.png')
path_img_in = os.path.join('.', 'tmp.png')
path_img_out = os.path.join('.', 'py-traiettorie_scena2o.png')

img1 = Image.open(path_img_in)
draw = ImageDraw.Draw(img1)

font = ImageFont.truetype(os.path.join('.', "Merriweather-BoldItalic.ttf"), 14)
draw.text((89, 75),'IFOPT','maroon',font=font)
draw.text((89,100),'NLOPT','blue',font=font)
draw.text((89,125),'ALGLIB','black',font=font)
    
img1.save(path_img_out)