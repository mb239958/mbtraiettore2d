# -*- coding: iso-8859-1 -*-
"""******************************************************************
 *   � 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   py-multi_traiettorie_alglib_scena1o.py 
 * 
 ******************************************************************"""
import numpy as np 
import matplotlib
matplotlib.use('Agg') # per potere lanciare lo script in terminale putty al di fuori di ambiente grafico
from matplotlib import pyplot as plt 
import os.path
from PIL import Image, ImageDraw, ImageEnhance, ImageFont

wlibreria = 'alglib'
wscena = 'scena1o'
wtitolo = 'Matteo Bigliardi UNIMORE - Analisi Traiettorie - ALGLIB'
#a_traiettoria = False
#np_alglib1 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_1.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
#np_alglib2 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_2.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
#np_alglib3 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_3.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
#np_alglib4 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_4.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
#np_alglib5 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_5.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')

a_traiettoria = True
np_traiettoria  = np.genfromtxt('traiettoria_xy_1.csv', dtype=[('x_ifopt','f8'),('y__ifopt','f8')], delimiter=';')
np_alglib1 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_1T.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
np_alglib2 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_2T.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
np_alglib3 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_3T.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
np_alglib4 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_4T.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
np_alglib5 = np.genfromtxt('OUT_alglib_traiettoria_scena1o_5T.csv', dtype=[('x_alglib','f8'),('y__alglib','f8')], delimiter=';')
if a_traiettoria:
    xt = []
    yt = []
    for punto in np_traiettoria:
        xt.append(punto[0])
        yt.append(punto[1])

x1 = []
y1 = []
for punto in np_alglib1:
    x1.append(punto[0])
    y1.append(punto[1])
x2 = []
y2 = []
for punto in np_alglib2:
    x2.append(punto[0])
    y2.append(punto[1])
x3 = []
y3 = []
for punto in np_alglib3:
    x3.append(punto[0])
    y3.append(punto[1])
x4 = []
y4 = []
for punto in np_alglib4:
    x4.append(punto[0])
    y4.append(punto[1])
x5 = []
y5 = []
for punto in np_alglib5:
    x5.append(punto[0])
    y5.append(punto[1])


plt.xlabel('x axis') 
plt.ylabel('y axis') 

circle1 = plt.Circle((1.20, 0.55), 0.12, color='r')
circle2 = plt.Circle((0.85, 0.65), 0.12, color='r')
circle3 = plt.Circle((0.40, 0.30), 0.12, color='r')
circle4 = plt.Circle((1.10, 0.82), 0.12, color='r')

fig, ax = plt.subplots() # note we must use plt.subplots, not plt.subplot
ax.set_xlim((0, 2))
ax.set_ylim((0, 1.55))

ax.add_artist(circle1)
ax.add_artist(circle2)
ax.add_artist(circle3)
ax.add_artist(circle4)

# apro se esiste csv con i punti da evidenziare (dove calcolo > 0.1)
if a_traiettoria:
    nome_file = 'OUT_' + wlibreria + '_' + wscena + '_T_posmaggioridi.csv'
else:    
    nome_file = 'OUT_' + wlibreria + '_' + wscena + '_posmaggioridi.csv'
if os.path.isfile(nome_file):
    foutputpos = open(nome_file, 'r')
    for line in foutputpos:
        xy = line.split(';')
        circle1 = plt.Circle((float(xy[0]), float(xy[1])), 0.015, color='orange')
        ax.add_artist(circle1)
    foutputpos.close()

plt.title(wtitolo)

if a_traiettoria:
    plt.plot(xt,yt, color='green', marker=',', linestyle='solid', linewidth=1, markersize=1)

plt.plot(x1,y1, color='blue', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x2,y2, color='black', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x3,y3, color='purple', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x4,y4, color='maroon', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x5,y5, color='lime', marker=',', linestyle='solid', linewidth=1, markersize=1)

plt.savefig('tmp.png')
path_img_in = os.path.join('.', 'tmp.png')

if a_traiettoria:
    path_img_out = os.path.join('.', 'py-multi_traiettorie_alglib_scena1oT.png')
else:
    path_img_out = os.path.join('.', 'py-multi_traiettorie_alglib_scena1o.png')

img1 = Image.open(path_img_in)
draw = ImageDraw.Draw(img1)

font = ImageFont.truetype(os.path.join('.', "Merriweather-BoldItalic.ttf"), 14)
if a_traiettoria :
    draw.text((89, 75),'Traiettoria desiderata','green',font=font)
img1.save(path_img_out) 
