# -*- coding: iso-8859-1 -*-
"""******************************************************************
 *   � 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   py-statistica_tempi_ottimizzazione.py
 * 
 ******************************************************************"""

import numpy as np
import csv

# prendo numero di righe "traiettoria" del primo file e considero solo i tempi ottimizzazione
# corrispondenti (stesso numero di riga dando per scontato si corrispondano ad 1 ad uno; quello 
# dei tempi ne ha di piu xchè l'ottimizzazione in realtà continua anche diopo scrittura degli OUT)

wtitolo = 'Medie valori su tutte le esecuzioni di ifopt scena1 senza ostacoli'
a_traiettoria = True
lfile = [
   {'ftra':'OUT_ifopt_traiettoria_scena1_1T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1_1T.txt'},
   {'ftra':'OUT_ifopt_traiettoria_scena1_2T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1_2T.txt'},
   {'ftra':'OUT_ifopt_traiettoria_scena1_3T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1_3T.txt'},
   {'ftra':'OUT_ifopt_traiettoria_scena1_4T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1_4T.txt'},
   {'ftra':'OUT_ifopt_traiettoria_scena1_5T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1_5T.txt'},
    ]

#wtitolo = 'Medie valori su tutte le esecuzioni di ifopt scena1 con ostacoli'
#a_traiettoria = True
#lfile = [
#   {'ftra':'OUT_ifopt_traiettoria_scena1o_1T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1o_1T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena1o_2T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1o_2T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena1o_3T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1o_3T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena1o_4T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1o_4T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena1o_5T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena1o_5T.txt'},
#    ]

#wtitolo = 'Medie valori su tutte le esecuzioni di ifopt scena2 senza ostacoli'
#a_traiettoria = True
#lfile = [
#   {'ftra':'OUT_ifopt_traiettoria_scena2_1T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2_1T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2_2T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2_2T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2_3T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2_3T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2_4T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2_4T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2_5T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2_5T.txt'},
#    ]

#wtitolo = 'Medie valori su tutte le esecuzioni di ifopt scena2 con ostacoli'
#a_traiettoria = True
#lfile = [
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_1T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_1T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_2T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_2T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_3T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_3T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_4T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_4T.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_5T.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_5T.txt'},
#    ]

#--------------------------------------------------------------------------------------------------------    

#wtitolo = 'Medie valori su tutte le esecuzioni di IFOPT scena2 con ostacoli'
#a_traiettoria = False
#lfile = [
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_1.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_1.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_2.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_2.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_3.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_3.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_4.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_4.txt'},
#   {'ftra':'OUT_ifopt_traiettoria_scena2o_5.csv','ftem':'OUT_ifopt_TempiOttimizzazione_scena2o_5.txt'},
#    ]

#wtitolo = 'Medie valori su tutte le esecuzioni di alglib scena1 senza ostacoli'
#a_traiettoria = False
#lfile = [
#   {'ftra':'OUT_alglib_traiettoria_scena1_1.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena1_1.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena1_2.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena1_2.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena1_3.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena1_3.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena1_4.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena1_4.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena1_5.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena1_5.txt'},
#    ]
 
#wtitolo = 'Medie valori su tutte le esecuzioni di alglib scena2 con ostacoli' 
#a_traiettoria = False
#lfile = [
#   {'ftra':'OUT_alglib_traiettoria_scena2o_1.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena2o_1.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena2o_2.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena2o_2.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena2o_3.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena2o_3.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena2o_4.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena2o_4.txt'},
#   {'ftra':'OUT_alglib_traiettoria_scena2o_5.csv','ftem':'OUT_alglib_TempiOttimizzazione_scena2o_5.txt'}, 
#   ]

lwmedia = []
lwmoda = []
lwnottimizzazioni = []
lwmassimo = []
lwminimo = []
lwmaggioredi = []

ho_aperto_csv_posizioni = False

for nf in lfile:
    ltempi=[]

    dpos_traiettorie={} # memorizzo tutte le posizioni per poi usarle
    conta_righe_traiettoria = 0    
    f = open(nf['ftra'], 'r')  # We need to re-open the file
    for line in f:
        conta_righe_traiettoria +=1
        xy = line.split(';')
        dpos_traiettorie[conta_righe_traiettoria]={}
        dpos_traiettorie[conta_righe_traiettoria]['x'] = float(xy[0])
        dpos_traiettorie[conta_righe_traiettoria]['y'] = float(xy[1])
        #print(line)
    f.close()
    #print('>>>' + str(dpos_traiettorie))
    #print('conta_righe_traiettoria :' + str(conta_righe_traiettoria))

    posizione = 1
    f = open(nf['ftem'], 'r')  
    for line in f:
        wline = repr(line).replace('\\n','').replace("'",'')
        d ={} 
        d['posizione'] = posizione
        d['tempo'] = float(str(wline))
        if posizione <= conta_righe_traiettoria:
            ltempi.append(d)
            #print(str(d))
        posizione +=1
    
    f.close()
    split = nf['ftem'].split('_') 
    # ['OUT', 'alglib', 'TempiOttimizzazione', 'scena1', '5.txt']
    wlibreria = split[1]
    wscena = split[3]
    wesecuzione = int(split[4].replace('.txt','').replace('T',''))
    print('-----------------------------------------')
    print(' libreria ' + wlibreria + ' ' + wscena + ' esecuzione num.' + str(wesecuzione))
    print(' ')


    if not ho_aperto_csv_posizioni:
        if a_traiettoria :
            wnome_file_pos_maggiori_di = 'OUT_' + wlibreria + '_' + wscena + '_T_posmaggioridi.csv'
        else:    
            wnome_file_pos_maggiori_di = 'OUT_' + wlibreria + '_' + wscena + '_posmaggioridi.csv'
        foutputpos = open(wnome_file_pos_maggiori_di, 'wb')
        ho_aperto_csv_posizioni = True

    wmedia = 0
    wmoda = 0
    wnottimizzazioni = 0
    wmassimo = 0
    wminimo = 0
    wmaggioredi = 0

    lw = []
    lw_posizioni = []
    for tempo in ltempi:
        if wminimo == 0 or wminimo>tempo['tempo']:
            wminimo = tempo['tempo']
        if wmassimo == 0 or wmassimo<tempo['tempo']:
            wmassimo = tempo['tempo']
        wnottimizzazioni = wnottimizzazioni + 1    
        if tempo['tempo'] > 0.1:
            wmaggioredi = wmaggioredi + 1
            lw_posizioni.append(tempo['posizione'])
        lw.append(tempo['tempo'])    
    
    data = np.asarray(lw, dtype=np.float32)
    wmedia = data.mean(0)            

    data2 = np.asarray(lw_posizioni, dtype=np.float32)
    wmedia_posizioni_magg01 = data2.mean(0)            

    # riduco il numero dei decimali nella lista dati
    # dalla uale ottenere la moda
    lwbis = []
    for k in lw:
        lwbis.append(round(k,3))

    # qui uso il set per verificare se ho valori doppi nelle liste
    #wset = set(lwbis)
    #lwbis2 = (list(wset))
    #if len(wset) == len(lwbis):
    #    print('Non ci sono duplicati!')
    #else:     
    #    k = len(lwbis) - len(wset)
    #    print('Ci sono duplicati!!!!!!!!!!!!!!!! ' + str(k))

    # qs metodo per calcolare la moda pare il piu corretto
    # (da il valore maggiormente usato)
    from collections import Counter
    data = Counter(lwbis)    
    wmoda = data.most_common(1)[0][0]
    #print(str(wmoda))

    # metodo per calcolare la moda non usato ma lo tengo
    # from scipy import stats
    # # a = np.array([[1, 3, 4, 2, 2, 7],
    # #             [5, 2, 2, 1, 4, 1],
    # #             [3, 3, 2, 2, 1, 1]])
    # m = stats.mode(data)
    # print(m)

    # questo metodo per calcolare la moda a volte restituisce eccezione
    #import statistics
    #wmoda = statistics.mode(lwbis)

    # qui sotto si ottiene la moda con una funzione ad hoc
    # ma a volte pare che dia dati errati... (la tengo qui non si sa mai)

    def find_max_mode(list1):
        list_table = statistics._counts(list1)
        len_table = len(list_table)

        if len_table == 1:
            max_mode = statistics.mode(list1)
        else:
            new_list = []
            for i in range(len_table):
                new_list.append(list_table[i][0])
            max_mode = max(new_list) # use the max value here
        return max_mode
    #wmoda = find_max_mode(lw)    

    print(' Media             {:10.6f}'.format(wmedia))
    print(' Moda              {:10.6f}'.format(wmoda))
    print(' N. ottimizzazioni {:10.6f}'.format(wnottimizzazioni))
    print(' Massimo           {:10.6f}'.format(wmassimo))
    print(' Minimo            {:10.6f}'.format(wminimo))
    print(' Maggiore di 0,1   {:10.6f}'.format(wmaggioredi))
    #print(' Media pos. > di 0,1.{:>18}'.format(int(wmedia_posizioni_magg01)))
    print(' Posizioni > di 0,1 {:>40}'.format(str(lw_posizioni)))

    for elp in lw_posizioni:
       #print('->>>' + str(dpos_traiettorie[elp]) )
       out = "%s;%s" % (dpos_traiettorie[elp]['x'],dpos_traiettorie[elp]['y'])
       foutputpos.write(out + "\n")

    lwmedia.append(wmedia)
    lwmoda.append(wmoda)
    lwnottimizzazioni.append(wnottimizzazioni)
    lwmassimo.append(wmassimo)
    lwminimo.append(wminimo)
    lwmaggioredi.append(wmaggioredi)

    print('------------------------------------------') 
    print('') 

data = np.asarray(lwmedia, dtype=np.float32)
wmedia = data.mean(0)            
data = np.asarray(lwmoda, dtype=np.float32)
wmoda = data.mean(0)            
data = np.asarray(lwnottimizzazioni, dtype=np.float32)
wnottimizzazioni = data.mean(0)            
data = np.asarray(lwmassimo, dtype=np.float32)
wmassimo = data.mean(0)            
data = np.asarray(lwminimo, dtype=np.float32)
wminimo = data.mean(0)            
data = np.asarray(lwmaggioredi, dtype=np.float32)
wmaggioredi = data.mean(0)            
           

print('------------------------------------------') 
print(wtitolo)

print(' Media             {:10.6f}'.format(round(wmedia,6)))
print(' Moda              {:10.6f}'.format(round(wmoda,6)))
print(' N. ottimizzazioni {:10.6f}'.format(round(wnottimizzazioni,6)))
print(' Massimo           {:10.6f}'.format(round(wmassimo,6)))
print(' Minimo            {:10.6f}'.format(round(wminimo,6)))
print(' Maggiore di 0,1   {:10.0f}'.format(round(wmaggioredi,6)))
print('------------------------------------------') 
print('') 

if ho_aperto_csv_posizioni:
    foutputpos.close()

print('fine elaborazione.')    
   