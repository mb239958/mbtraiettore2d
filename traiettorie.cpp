/******************************************************************
 *   � 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   traiettorie.cpp (incapsula py-traiettorie.py)
 * 
 ******************************************************************/
#include <Python.h>
int main()
{
   Py_Initialize();
   PyRun_SimpleString("import numpy as np ");
   PyRun_SimpleString("import matplotlib");
   PyRun_SimpleString("matplotlib.use('Agg')");
   PyRun_SimpleString("from matplotlib import pyplot as plt ");

   PyRun_SimpleString("np_ifopt = np.genfromtxt('traiettoria_ifopt.csv', delimiter=';')");
   PyRun_SimpleString("np_nlopt = np.genfromtxt('traiettoria_nlopt.csv', delimiter=';')");
   PyRun_SimpleString("np_alglib = np.genfromtxt('traiettoria_alglib.csv', delimiter=';')");

   PyRun_SimpleString("plt.title('Matteo Bigliardi UNIMORE - Analisi Traiettorie - ifopt/nlopt/alglib') ");
   PyRun_SimpleString("plt.xlabel('x axis') ");
   PyRun_SimpleString("plt.ylabel('y axis') ");
   PyRun_SimpleString("for punto in np_ifopt: plt.plot(punto[0],punto[1])");
   PyRun_SimpleString("plt.savefig('xkcd.png')");
   Py_Exit(0);
   return 0;
}