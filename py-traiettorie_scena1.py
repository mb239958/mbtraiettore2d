# -*- coding: iso-8859-1 -*-
"""******************************************************************
 *   � 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   py-traiettorie_scena1.py 
 * 
 ******************************************************************"""
import numpy as np 
import matplotlib
matplotlib.use('Agg') # per potere lanciare lo script in terminale putty al di fuori di ambiente grafico
from matplotlib import pyplot as plt 
import os.path
from PIL import Image, ImageDraw, ImageEnhance, ImageFont

wlibreria = 'ifopt/nlopt/alglib'
wscena = 'scena1'
wtitolo = 'Matteo Bigliardi UNIMORE - Analisi Traiettorie - IFOPT/NLOPT/ALGLIB'
np_ifopt  = np.genfromtxt('OUT_ifopt_traiettoria_scena1.csv', dtype=[('x_ifopt','f8'),('y__ifopt','f8')], delimiter=';')
np_nlopt  = np.genfromtxt('OUT_nlopt_traiettoria_scena1.csv', dtype=[('x_ifopt','f8'),('y__ifopt','f8')], delimiter=';')
np_alglib = np.genfromtxt('OUT_alglib_traiettoria_scena1.csv', dtype=[('x_ifopt','f8'),('y__ifopt','f8')], delimiter=';')

x = []
y = []
for punto in np_ifopt:
    x.append(punto[0])
    y.append(punto[1])
x1 = []
y1 = []
for punto in np_nlopt:
    x1.append(punto[0])
    y1.append(punto[1])
x2 = []
y2 = []
for punto in np_alglib:
    x2.append(punto[0])
    y2.append(punto[1])


plt.xlabel('x axis') 
plt.ylabel('y axis') 

fig, ax = plt.subplots() # note we must use plt.subplots, not plt.subplot
ax.set_xlim((0, 2))
ax.set_ylim((0, 1.55))

plt.title(wtitolo) 

plt.plot(x,y, color='maroon', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x1,y1, color='blue', marker=',', linestyle='solid', linewidth=1, markersize=1)
plt.plot(x2,y2, color='black', marker=',', linestyle='solid', linewidth=1, markersize=1)

plt.savefig('tmp.png')
path_img_in = os.path.join('.', 'tmp.png')
path_img_out = os.path.join('.', 'py-traiettorie_scena1.png')

img1 = Image.open(path_img_in)
draw = ImageDraw.Draw(img1)

font = ImageFont.truetype(os.path.join('.', "Merriweather-BoldItalic.ttf"), 14)
draw.text((89, 75),'IFOPT','maroon',font=font)
draw.text((89,100),'NLOPT','blue',font=font)
draw.text((89,125),'ALGLIB','black',font=font)
    
img1.save(path_img_out)
